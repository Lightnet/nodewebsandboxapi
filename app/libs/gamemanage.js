/*
    Project Name: Node Web Sandbox API
    Link:https://bitbucket.org/Lightnet/nodewebsandboxapi
    Created By: Lightnet
    License: Creative Commons [Note there multiple Licenses]
    Please read the readme.txt file for more information.
  
    Information:
    
*/
//declare var module: any;
var Globals;
(function (Globals) {
    Globals.m_Name = "Game Manage";
})(Globals = exports.Globals || (exports.Globals = {}));
var Manage = (function () {
    //console.log("init manage");
    function Manage() {
        //public static MyInstance:any;
        this.self = this;
        if (Globals.m_Instance == null) {
            Globals.m_Instance = this;
            this.id = Math.random();
        }
        else if (Globals.m_Instance != this) {
            this.self = Globals.m_Instance;
        }
        console.log("init manage" + this.id);
        //console.log(module);
        //console.log(Globals);
    }
    return Manage;
})();
var Data = (function () {
    //console.log("init manage");
    function Data() {
        console.log("init manage");
    }
    return Data;
})();
(module).exports = Manage;
/*
class Manage{
    constructor(){
        console.log("Init Manage");
    }
}

export Manage;
*/ 
