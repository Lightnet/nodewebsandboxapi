/**
 * ...
 * @author Lightnet
 */
	//==============================================
	// variables
	//==============================================
	var scene = new THREE.Scene();
	//var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
	var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000000 );
	var raycaster = new THREE.Raycaster();
	var traceobjects = [];
	var mouse = new THREE.Vector2();
	var INTERSECTED;

	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;
	var cameraOrtho, sceneOrtho;
	cameraOrtho = new THREE.OrthographicCamera( -WIDTH / 2, WIDTH / 2, HEIGHT / 2, - HEIGHT / 2, -1, 20);
	cameraOrtho.position.z = 1;
	cameraOrtho.updateProjectionMatrix();
	sceneOrtho = new THREE.Scene();

	var renderer;
	renderer = new THREE.WebGLRenderer({ alpha: true });
	//renderer = new THREE.WebGLRenderer();
	//renderer = new THREE.CanvasRenderer();

	//renderer.setClearColorHex( 0xffffff, 1 );
	renderer.setClearColor(0xffffff, 0);
	renderer.setSize( window.innerWidth, window.innerHeight );

	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
	var cube = new THREE.Mesh( geometry, material );
	var mesh = cube;
	THREEx.WindowResize(renderer, camera);
	scene.add( cube );

	camera.position.z = 50;


	var _render;// = document.getElementById("render");



	var orbitcontrol = new THREE.OrbitControls( camera );
	//orbitcontrol.addEventListener( 'change', render );
	//var _render = document.getElementById("render");
	var trackcontrol; //= new THREE.TrackballControls(camera, _render);



	var geometry = new THREE.PlaneBufferGeometry(500000, 500000, 32, 32);
    var material = new THREE.MeshBasicMaterial({ color: 0xb9b9b9, wireframe: true });
	planepoint3d = new THREE.Mesh(geometry, material);
    planepoint3d.rotation.x = (90 * Math.PI / 180) * -1;
    planepoint3d.name = "plane";
    scene.add(planepoint3d);


	function MouseClickObject(event){
		console.log(event);
		console.log("MouseClickObject...");

		//var vector = new THREE.Vector3( mouse.x, mouse.y, 1 );
		//projector.unprojectVector( vector, camera );
		//raycaster.set(camera.position, vector.sub(camera.position).normalize());
		raycaster.setFromCamera( mouse, camera );

		var intersects = raycaster.intersectObjects(scene.children );
		console.log("LEN:"+intersects.length);
		if ( intersects.length > 0 ) {
			if ( INTERSECTED != intersects[ 0 ].object ) {
				//console.log(intersects[ 0 ].object.position);
				console.log(intersects[ 0 ].object);
			}
		}
	}

	function MouseMove(){
		//console.log("MouseMove...");
		mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
		mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
	}

	renderer.domElement.addEventListener("mousemove", MouseMove);
	renderer.domElement.addEventListener("mousedown", MouseClickObject);



	var ballTexture = THREE.ImageUtils.loadTexture( 'assets/textures/tmp_cube.png' );
	//console.log(ballTexture );
	//var ballMaterial = new THREE.SpriteMaterial( { map: ballTexture, useScreenCoordinates: true, alignment: THREE.SpriteAlignment.topLeft  } );
	var ballMaterial = new THREE.SpriteMaterial( { map: ballTexture, useScreenCoordinates: true} );
	var sprite = new THREE.Sprite( ballMaterial );
	//sprite.position.set( 50, 50, 0 );
	sprite.position.set( 100, 100, 0 );
	sprite.scale.set( 32, 32, 1.0 ); // imageWidth, imageHeight
	//mesh.callback = function() { console.log( this.name ); }
	sprite.callback = function() {
		console.log( this.name );
		console.log("callback here click");
	};
	//uiobjects.push(sprite);
	sceneOrtho.add( sprite );










	//==============================================
	// render
	//==============================================

	var render = function () {
		requestAnimationFrame( render );

		//if(orbitcontrol !=null){
			//orbitcontrol.update();
			//orbitcontrol.enabled = true;
		//}

		if(trackcontrol !=null){
			trackcontrol.update();
			trackcontrol.enabled = true;
		}

		//cube.rotation.x += 0.1;
		//cube.rotation.y += 0.1;
		renderer.clear();

		renderer.render( sceneOrtho, cameraOrtho );
		renderer.clearDepth();
		renderer.render(scene, camera);
	};








	//==============================================
	// start load
	//==============================================

	function LoadScene(){
		//document.body.appendChild( renderer.domElement );
		_render = document.getElementById("render");
		_render.appendChild(renderer.domElement );
		trackcontrol = new THREE.TrackballControls(camera, _render);

		render();
	}
	window.addEventListener("load", LoadScene);

	//==============================================
	//end load
	//==============================================
