/*
	Project Name: Node Web Sandbox
	Link:https://bitbucket.org/Lightnet/nodewebsandbox

	Information: To create a bot and rpg system with the module plug-in build prototype.
	Currently work in progress builds.

	Notes: Working toward multiple builds to keep simple and stable to run host server.

	Please read the readme.txt file for more information.

	Main Scripts Complile

*/

/*
 Information:
	 -This load the script and write them into one script. But depend on the settings.
*/

//declare var THREEx;

// <reference path="../DefinitelyTyped/threejs/three.d.ts" />
// <reference path="../DefinitelyTyped/yui/yui.d.ts" />
// <reference path="../DefinitelyTyped/knockout/knockout.d.ts" />

/// <reference path="./threejsbase.ts" />
/// <reference path="./threejsgame.ts" />
