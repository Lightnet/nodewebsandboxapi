/*
    Project Name: Node Web Sandbox API
    Link:https://bitbucket.org/Lightnet/nodewebsandboxapi
    Created By: Lightnet
    License: Please read the readme.txt file for more information.
  
    Information:
    
*/

/* global mongoose  */
/* global nano */


/* global map */
/* global terrain */
/* global terrainvoxel */

var express = require('express');

var manage = require('../../app/libs/gamemanage');
try  {
    var manageplugin = require('../../app/libs/manageplugin');
    //console.log(manageplugin);
} catch (err) {	
	console.log(err);
}

var io;
var socket;
var db;

//===============================================
// Config
//===============================================

nano.db.create('terrain');
nano.db.create('map');
nano.db.create('terrainvoxel');

terrain = nano.use('terrain');
map = nano.use('map');
terrainvoxel = nano.use('terrainvoxel');

var feed = terrainvoxel.follow({since: "now"});
feed.on('change', function (change) {
  console.log("change: ", change);
});
feed.follow();


//===============================================
// Config
//===============================================

module.exports._config = require('./index.json');
//===============================================
// Route
//===============================================

module.exports.setroute = function(routes,app){
    
    //console.log(nano);
    //console.log(mongoose);
    
    //console.log(mongoose);
	//console.log('node bot system');
	//display the index forum
	//console.log(manageplugin);
	try  {
	    manageplugin.addAppView(app, __dirname + '/views');
	} catch (err) {	
	    console.log(err);
    }

	app.use(express.static(__dirname + '/public'));
	
    //routes.get('/game', function (req, res) {
    routes.get('/', function (req, res) {
       res.contentType('text/html');
       //res.send('Hello World!');
       res.render('game', {user: req.user});
	});
	
	routes.get('/tg', function (req, res) {
       res.contentType('text/html');
       //res.send('Hello World!');
       res.render('test', {user: req.user});
	});
};

//===============================================
// Socket.io
//===============================================

module.exports.socket_connect = function(_io, _socket,_db){
  //console.log("connect! bot script!");
  
  io = _io;
  socket = _socket;
  socket = _socket;
  socket.on('threejs', function (data) {
    //console.log(data);
    
    if(data['action'] !=null){
        console.log("Action:"+data['action']);
        console.log("idhash:"+data['idhash']);
        if(data['action'] == "savesceneobject"){
            SaveSceneObject(data);
        }
        if(data['action'] == "deletesceneobject"){
            DeleteSceneObject(data);
        }
        
        if(data['action'] == "loadsceneobjects"){
            LoadSceneobjects(data,socket);
        }
        
        if(data['action'] == "clearsceneobjects"){
            ClearSceneobjects(data);
        }
    }
    
  });
  
};

function ClearSceneobjects(obj){
    terrainvoxel.list(function(err, body) {
        console.log("ID ROWS");
        if (!err) {
            body.rows.forEach(function(doc) {
                console.log(doc);
            	terrainvoxel.get(doc.id,  function(err, body2) {
            	     if (!err){
            	        console.log("info");
                        console.log(body2);
            	     }
            	});
            });
        }
    });
}

function LoadSceneobjects(obj,_socket){
    
    terrainvoxel.list(function(err, body) {
        console.log("ID ROWS");
        if (!err) {
            body.rows.forEach(function(doc) {
                console.log(doc);
            	terrainvoxel.get(doc.id,  function(err, body2) {
            	     if (!err){
            	        console.log("DATA:");
                        console.log(body2);
                        
                         var objectarray ={
	                    	type:body2['type'],
		                    idname:body2['idname'],
                    		idhash:body2['idhash'],
                    		
                    		name:body2['name'],
                    		position: body2['position'],
                    		rotation: body2['rotation'],
                    		scale:body2['scale'],
                    		
                    		params:body2['params']
                    	}
                    	objectarray.action = "addsceneobject";
                        _socket.emit('threejs',objectarray);
                        
            	     }
            	});
            });
        }
    });
}

function DeleteSceneObject(obj,_socket){
    var idhash = obj['idhash'];
    console.log(idhash);
   	terrainvoxel.get(idhash, function(err, body) {
        if (!err) {
            console.log("DELETE CHECK?");
            console.log(body);
            terrainvoxel.destroy(idhash, body._rev, function(err2, body2) {
                if (!err2){
                    console.log("data:");
                    console.log(body2);
                    //body2.id;
                     var objectarray ={
                		idhash:body2.id
                	}
                	objectarray.action = "deletesceneobject";
                    _socket.emit('threejs',objectarray);
                }
            });
        }else{
            console.log("doesn't exist or been deleted!");
        }
        //terrainvoxel.destroy('rabbit', '3-66c01cdf99e84c83a9b3fe65b88db8c0', function(err, body) {
            //if (!err)
                //console.log(body);
        //});
    }); 
    
}

function SaveSceneObject(obj){ //work fine
    
    var objectarray ={
		type:obj['type'],
		idname:obj['idname'],
		idhash:obj['idhash'],
		
		name:obj['name'],
		position: obj['position'],
		rotation: obj['rotation'],
		scale:obj['scale'],
		
		params:obj['params']
	}
	
	
	terrainvoxel.get(obj['idhash'], function(err, body) {
	    console.log(err);
	    console.log(body);
	    
        //if (!err) {
            //console.log(body);
        //}
        //console.log(err);
        if(err !=null){
            //if file doesn't exist or delete
            //add object
            if((err.reason == 'no_db_file')||(err.reason == 'deleted')||(err.reason == 'missing')){
                console.log("creating file block");
                terrainvoxel.insert(objectarray,obj['idhash'] , function(err2, body2) {
                    if (!err2){
                        console.log(body2);
                    }
                });
            }
        }else{
            //update object from rev if exist
            objectarray._rev = body._rev;
             terrainvoxel.insert(objectarray, obj['idhash'], function(err2, body2) {
                if (!err2){
                    console.log(body2);
                }
                //console.log('FINISH UPDATE?');
            });
            //console.log(body);
            
        }
        //console.log(body);
        //console.log(body);
        
        console.log('FINISH GET...');
    });
	
	
	
	
	
	
	
	/*
	nano.db.list(function(err, body) {
        // body is an array
        body.forEach(function(db) {
            console.log(db);
        });
    });
	*/
	/*
	nano.db.get('terrainvoxel', function(err, body) {
        if (!err) {
            console.log(body);
        }
        //console.log(body);
        //console.log(body);
    });
    */
    
    
    /*
    terrainvoxel.list(function(err, body) {
        console.log("ID ROWS");
        if (!err) {
            body.rows.forEach(function(doc) {
                console.log(doc);
                	terrainvoxel.get(doc.id,  function(err, info) {
                	     if (!err){
                	        console.log("info");
                            console.log(info);
                	     }
                	});
            });
        }
    });
	*/
	
    /*
    terrainvoxel.insert(objectarray, function(err, body) {
                if (!err){
                    console.log(body);
                }
            
            });
            */
    /*
    terrainvoxel.get(objectarray,'block', function(err, body) {
        console.log("ITEM?");
        if (!err){
            console.log("body");
            console.log(body);
        }
        //console.log(err);
        //console.log(body);
        if(err.reason =='missing'){
            
            terrainvoxel.insert(objectarray,'block' , function(err, body) {
                if (!err){
                    console.log(body);
                }
            
            });
        }
    });
    */
    
    //console.log(terrainvoxel);
    
    /*
    terrainvoxel.list(function(err, body) {
        // body is an array
        console.log(body);
        console.log("????");
        for(var i = 0; i < body.rows.length; i++){
            console.log(body.rows[i]);
        }
    });
    */
    
}

function GetMap(){
     terrainvoxel.list(function(err, body) {
        console.log("ID ROWS");
        if (!err) {
            body.rows.forEach(function(doc) {
                console.log(doc);
            	terrainvoxel.get(doc.id,  function(err, info) {
            	     if (!err){
            	        console.log("info");
                        console.log(info);
            	     }
            	});
            });
        }
    });
    
}


module.exports.socket_disconnect = function(io, socket,db){
 //console.log("disconnect! bot script!");
};

