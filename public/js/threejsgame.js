var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="./threejsbase.ts" />
//declare var Window:any;
var ThreejsGame = (function (_super) {
    __extends(ThreejsGame, _super);
    function ThreejsGame() {
        _super.call(this);
        this.bdisplayscene = true;
        this.objectphysics = [];
        this.control_x = 0;
        this.control_y = 0;
        this.up = false;
        this.down = false;
        this.right = false;
        this.left = false;
        this.bplayercontrol = true;
        this.traceobjects = [];
        this.Mouse = new THREE.Vector2();
        this.raycaster = new THREE.Raycaster();
        console.log("init game");
    }
    ThreejsGame.prototype.self = function () {
        return this;
    };
    // this will trigger event on load listener
    // put setup code here
    ThreejsGame.prototype.setup = function () {
        var _this = this;
        _super.prototype.setup.call(this);
        this.manager = new THREE.LoadingManager();
        this.manager.onProgress = function (item, loaded, total) {
            console.log(item, loaded, total);
        };
        //this.init_pointer();
        this.renderer.setClearColor(0xffffff, 1);
        this.renderer.autoClear = false;
        this.setup_ground();
        console.log("setup here...");
        var axes = new THREE.AxisHelper(500);
        this.scene.add(axes);
        var geometry = new THREE.BoxGeometry(200, 150, 50);
        geometry.faces[0].color.setHex(0x890202);
        geometry.faces[1].color.setHex(0x890202);
        geometry.faces[2].color.setHex(0x9e0208);
        geometry.faces[3].color.setHex(0x9e0208);
        geometry.faces[4].color.setHex(0xFF0000);
        geometry.faces[5].color.setHex(0x333333);
        var material = new THREE.MeshBasicMaterial({ vertexColors: THREE.FaceColors });
        var side = 1;
        var object = new THREE.Mesh(geometry, material);
        object.position.x = 0;
        object.position.y = -5;
        object.position.z = 0;
        //object.position.x = Math.cos(side * Math.PI/2) * 200;
        //object.position.z = -Math.sin(side * Math.PI/2) * 200;
        //object.rotation.y = (side+1) * Math.PI/2;
        this.scene.add(object);
        //mouse
        this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
        this.controls.handleResize();
        //this.renderer.domElement.addEventListener('mousedown', this.onDocumentMouseDown, false);
        this.renderer.domElement.addEventListener('mousedown', MouseDown, false);
        //this.renderer.domElement.addEventListener('mouseup', this.onDocumentMouseUp, false);
        this.renderer.domElement.addEventListener('mouseup', MouseUp, false);
        this.renderer.domElement.addEventListener('mousemove', MouseMove, false);
        this.renderer.domElement.addEventListener('mouseout', MouseOut, false);
        var self = this;
        function MouseDown(event) {
            console.log("down");
            self.onDocumentMouseDown(event);
        }
        function MouseUp(event) {
            self.onDocumentMouseUp(event);
        }
        function MouseMove(event) {
            self.onDocumentMouseMove(event);
        }
        function MouseOut(event) {
            self.onDocumentMouseOut(event);
        }
        //console.log(this.controls);
        // RESIZE
        window.addEventListener('resize', function () { return _this.resizeHandler(); }, false);
        //document.addEventListener('keydown', this.KeyDownControl, false);
        //document.addEventListener('keyup', this.KeyUpControl, false);
        //createplayer_object(this.scene, this.world,this.objectphysics);
        this.loadobj();
    };
    ThreejsGame.prototype.loadobj = function () {
        var self = this;
        var loader = new THREE.OBJLoader(this.manager);
        loader.load('assets/models/building_space_blocks.obj', function (object) {
            object.traverse(function (child) {
                if (child instanceof THREE.Mesh) {
                }
            });
            //object.position.y = - 80;
            self.scene.add(object);
        }, self.onProgress, self.onError);
    };
    ThreejsGame.prototype.onProgress = function (xhr) {
        if (xhr.lengthComputable) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            percentComplete = Math.round(percentComplete, 2);
            console.log(percentComplete + '% downloaded');
        }
    };
    ThreejsGame.prototype.onError = function (xhr) {
    };
    //87 = up
    //83 = down
    //65 = left
    //68 = right
    //
    ThreejsGame.prototype.KeyDownControl = function (event) {
        console.log('test');
        console.log(event.keyCode);
        // if(){
        // }
    };
    ThreejsGame.prototype.KeyUpControl = function (event) {
        console.log(event.keyCode);
    };
    ThreejsGame.prototype.showlog = function () {
        console.log("test");
    };
    ThreejsGame.prototype.onDocumentMouseDown = function (event) {
        event.preventDefault();
        console.log(this);
        this.raycaster.setFromCamera(this.Mouse, this.camera);
        var intersects = this.raycaster.intersectObjects(this.scene.children);
        if (intersects.length > 0) {
            if (this.INTERSECTED != intersects[0].object) {
                //console.log(intersects[ 0 ].object.position);
                console.log(intersects[0].object);
            }
        }
        console.log("onDocumentMouseDown");
    };
    ThreejsGame.prototype.onDocumentMouseUp = function (event) {
        event.preventDefault();
        console.log("onDocumentMouseUp");
    };
    ThreejsGame.prototype.onDocumentMouseOut = function (event) {
        event.preventDefault();
    };
    //var self = this;
    ThreejsGame.prototype.onDocumentMouseMove = function (event) {
        event.preventDefault();
        //console.log(this);
        var self = this;
        if (self.Mouse != null) {
            self.Mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
            self.Mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
        }
        //console.log("move"+self.Mouse.x + ":" + self.Mouse.y);
        //console.log("move"+self());
        //console.log(this.self());
    };
    //render the scene and camera since there are layer coding will be here
    // to draw the scene and  (hud / ui)
    ThreejsGame.prototype.render = function () {
        _super.prototype.render.call(this); //callback to render it
        if (this.controls != null) {
            this.controls.update();
        }
        //console.log(this);
        //this.renderer.setClearColor( 0xffffff, 1 );
        this.renderer.clear();
        if (this.bdisplayscene == true) {
            this.renderer.render(this.scene, this.camera);
        }
        this.renderer.clearDepth();
        //console.log("testing...");
        if (this.world != null) {
            this.world.step(this.timeStep);
        }
        for (var i = 0; i < this.objectphysics.length; i++) {
            this.objectphysics[i].position.copy(this.objectphysics[i].shape.position);
            //console.log(this.objectphysics[i].update);
            if (this.objectphysics[i].update) {
                this.objectphysics[i].update();
            }
        }
        /*
        for(i = 0; i < this.objectphysics.length; i++){
              this.objectphysics[i].position.set(
                    this.objectphysics[i].shape.position.x,
                    this.objectphysics[i].shape.position.y,
                    this.objectphysics[i].shape.position.z
              );
        }
        */
        //this.objectphysics.push(player);
        //console.log(this.objectphysics.length);
    };
    ThreejsGame.prototype.resizeHandler = function () {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        //console.log(this.camera);
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.controls.handleResize();
    };
    // cannon
    ThreejsGame.prototype.setup_ground = function () {
        // Create a plane
        var groundShape = new CANNON.Plane();
        var groundBody = new CANNON.RigidBody(0, groundShape);
        groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);
        this.world.add(groundBody);
        //this.create_physictest();
        //createphysics_object(this.scene,this.world,this.objectphysics);
    };
    ThreejsGame.prototype.create_physictest = function () {
        console.log("add scene object...");
        var geometry = new THREE.BoxGeometry(1, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var player = new THREE.Mesh(geometry, material);
        player.position.set(0, 3, 0);
        var mass = 5, radius = 1;
        var sphereShape = new CANNON.Sphere(radius);
        var sphereBody = new CANNON.RigidBody(mass, sphereShape);
        player.shape = sphereBody;
        sphereBody.position.set(0, 10, 0);
        this.world.add(sphereBody);
        this.scene.add(player);
        this.objectphysics.push(player);
        //objectphysics
        //console.log(player.shape);
        //console.log(this.objectphysics.length);
        //console.log(this.objectphysics.length);
    };
    ThreejsGame.prototype.init_pointer = function () {
        console.log("init no mouse pointer"); //hide mouse
        var _document = document; //this will try to remove error from typescript
        //var blocker = _document.getElementById( 'blocker' );
        //var instructions = _document.getElementById( 'instructions' );
        var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
        function reqestpointer() {
            element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
            if (/Firefox/i.test(navigator.userAgent)) {
                var fullscreenchange = function (event) {
                    if (_document.fullscreenElement === element || _document.mozFullscreenElement === element || _document.mozFullScreenElement === element) {
                        _document.removeEventListener('fullscreenchange', fullscreenchange);
                        _document.removeEventListener('mozfullscreenchange', fullscreenchange);
                        element.requestPointerLock();
                    }
                };
                document.addEventListener('fullscreenchange', fullscreenchange, false);
                document.addEventListener('mozfullscreenchange', fullscreenchange, false);
                element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
                element.requestFullscreen();
            }
            else {
                element.requestPointerLock();
                console.log("click requestPointerLock");
            }
        }
        var idfocus = document.getElementById('focus');
        idfocus.addEventListener("click", function (e) {
            reqestpointer();
        }, false);
        //document.addEventListener("keydown", doKeyDown, false);
        document.addEventListener("keyup", doKeyDown, false);
        //window.addEventListener( "keypress", doKeyDown, false );
        function doKeyDown(e) {
            //console.log( e.keyCode );
            if (e.keyCode == 220) {
                reqestpointer();
            }
        }
        if (havePointerLock) {
            var element = _document.body; //this will try to remove error from typescript
            var pointerlockchange = function (event) {
                if (_document.pointerLockElement === element || _document.mozPointerLockElement === element || _document.webkitPointerLockElement === element) {
                }
                else {
                    //fpscontrol.enabled = false;
                    console.log(" fpscontrol false");
                }
            };
            var pointerlockerror = function (event) {
                //instructions.style.display = '';
            };
            // Hook pointer lock state change events
            document.addEventListener('pointerlockchange', pointerlockchange, false);
            document.addEventListener('mozpointerlockchange', pointerlockchange, false);
            document.addEventListener('webkitpointerlockchange', pointerlockchange, false);
            document.addEventListener('pointerlockerror', pointerlockerror, false);
            document.addEventListener('mozpointerlockerror', pointerlockerror, false);
            document.addEventListener('webkitpointerlockerror', pointerlockerror, false);
        }
        else {
        }
    };
    //===================================================================
    //player handlers section
    //===================================================================
    ThreejsGame.prototype.CreatePlayerObject = function (data) {
        var geometry = new THREE.BoxGeometry(1, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var player = new THREE.Mesh(geometry, material);
        player.userid = data['userid'];
        player.name = "playercube";
        this.scene.add(player);
        this.currentplayer = player;
    };
    return ThreejsGame;
})(ThreejsBase);
//init game
var game = new ThreejsGame();
//console.log(game);
