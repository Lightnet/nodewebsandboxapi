var Globals;
(function (Globals) {
    Globals.m_Name = "Game Manage";
})(Globals = exports.Globals || (exports.Globals = {}));
var instanceworld = (function () {
    //console.log("init manage");
    function instanceworld() {
        //public static MyInstance:any;
        this.self = this;
        this.modulelist = [];
        if (Globals.m_Instance == null) {
            Globals.m_Instance = this.self;
            this.id = Math.random();
        }
        else if (Globals.m_Instance != this.self) {
            this.self = Globals.m_Instance;
        }
        //console.log("init manage plugin:"+this.id);
        //console.log(module);
        //console.log(Globals);
        return this;
    }
    instanceworld.prototype.addModule = function (_module) {
        //console.log("Added Module...");
        this.modulelist.push(_module);
    };
    instanceworld.prototype.getID = function () {
        return this.id;
    };
    return instanceworld;
})();
exports = (module).exports = new instanceworld();
