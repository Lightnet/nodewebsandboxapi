/*
      Project Name: Node Web Sandbox
      Link:https://bitbucket.org/Lightnet/nodewebsandbox

      Information: To create a bot and rpg system with the module plug-in build prototype.
      Currently work in progress builds.

*/
// <reference path="../DefinitelyTyped/threejs/three.d.ts" />
/// <reference path="./threejsbase.ts" />
declare var THREEx;
//declare var Window:any;

class ThreejsGame extends ThreejsBase{

	controls:any;
	bdisplayscene:boolean = true;
	currentplayer:any;
	objectphysics:any = [];

	control_x:number = 0;
	control_y:number = 0;

	up:boolean = false;
	down:boolean = false;
	right:boolean = false;
	left:boolean = false;

	bplayercontrol:boolean = true;


	traceobjects:any = [];
	Mouse:any = new THREE.Vector2();
	raycaster:any = new THREE.Raycaster();
	INTERSECTED:any;
	manager :any;

	constructor(){
		super();
		console.log("init game");
	}

	self(){
		return this;
	}

	// this will trigger event on load listener
	// put setup code here
	setup(){
		super.setup();
		this.manager = new THREE.LoadingManager();
		this.manager.onProgress = function ( item, loaded, total ) {
			console.log( item, loaded, total );
		};
		//this.init_pointer();
		this.renderer.setClearColor( 0xffffff, 1 );
		this.renderer.autoClear = false;

		this.setup_ground();

		console.log("setup here...");
		var axes = new THREE.AxisHelper(500);
		this.scene.add(axes);

		var geometry = new THREE.BoxGeometry(200, 150, 50);
		geometry.faces[0].color.setHex( 0x890202 );
		geometry.faces[1].color.setHex( 0x890202 );
		geometry.faces[2].color.setHex( 0x9e0208 );
		geometry.faces[3].color.setHex( 0x9e0208 );
		geometry.faces[4].color.setHex( 0xFF0000 );
		geometry.faces[5].color.setHex( 0x333333 );

		var material = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors } );
		var side = 1;
		var object = new THREE.Mesh( geometry, material );
		object.position.x = 0;
		object.position.y = -5;
		object.position.z = 0;
		//object.position.x = Math.cos(side * Math.PI/2) * 200;
		//object.position.z = -Math.sin(side * Math.PI/2) * 200;
		//object.rotation.y = (side+1) * Math.PI/2;
		this.scene.add( object );

		//mouse
		this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
		this.controls.handleResize();
		//this.renderer.domElement.addEventListener('mousedown', this.onDocumentMouseDown, false);
		this.renderer.domElement.addEventListener('mousedown', MouseDown, false);
		//this.renderer.domElement.addEventListener('mouseup', this.onDocumentMouseUp, false);
		this.renderer.domElement.addEventListener('mouseup', MouseUp, false);
		this.renderer.domElement.addEventListener('mousemove', MouseMove, false);
		this.renderer.domElement.addEventListener('mouseout', MouseOut, false);

		var self = this;
		function MouseDown(event){
			console.log("down");
			self.onDocumentMouseDown(event);
		}

		function MouseUp(event){
			self.onDocumentMouseUp(event);
		}

		function MouseMove(event){
			self.onDocumentMouseMove(event);
		}

		function MouseOut(event){
			self.onDocumentMouseOut(event);
		}

		//console.log(this.controls);
		// RESIZE
		window.addEventListener('resize',()=> this.resizeHandler(), false);

		//document.addEventListener('keydown', this.KeyDownControl, false);
		//document.addEventListener('keyup', this.KeyUpControl, false);
		//createplayer_object(this.scene, this.world,this.objectphysics);
		this.loadobj();
	}

	loadobj(){
		var self = this;
		var loader = new THREE.OBJLoader( this.manager );
		loader.load( 'assets/models/building_space_blocks.obj', function ( object ) {
			object.traverse( function ( child ) {
				if ( child instanceof THREE.Mesh ) {

					//child.material.map = texture;
				}
			} );
			//object.position.y = - 80;
			self.scene.add( object );
		}, self.onProgress, self.onError );
	}

	onProgress( xhr ) {
		if ( xhr.lengthComputable ) {
			var percentComplete = xhr.loaded / xhr.total * 100;
			percentComplete = Math.round(percentComplete, 2);
			console.log( percentComplete + '% downloaded' );
		}
	}

	onError( xhr ) {

	}

	//87 = up
	//83 = down
	//65 = left
	//68 = right
	//
	KeyDownControl(event){
		console.log('test');
		console.log( event.keyCode );
		// if(){

		// }
	}

	KeyUpControl(event){
		console.log( event.keyCode );
	}

	showlog(){
		console.log("test");
	}

	onDocumentMouseDown(event) {
		event.preventDefault();
		console.log(this);

		this.raycaster.setFromCamera( this.Mouse, this.camera );

		var intersects = this.raycaster.intersectObjects(this.scene.children );
		if ( intersects.length > 0 ) {
			if ( this.INTERSECTED != intersects[ 0 ].object ) {
				//console.log(intersects[ 0 ].object.position);
				console.log(intersects[ 0 ].object);
			}
		}

		console.log("onDocumentMouseDown");
	}
	onDocumentMouseUp(event) {
		event.preventDefault();
		console.log("onDocumentMouseUp");
	}
	onDocumentMouseOut(event) {
		event.preventDefault();
	}
	//var self = this;
	onDocumentMouseMove(event) {
		event.preventDefault();
		//console.log(this);
		var self = this;
		if(self.Mouse !=null){
			self.Mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
			self.Mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
		}
		//console.log("move"+self.Mouse.x + ":" + self.Mouse.y);
		//console.log("move"+self());
		//console.log(this.self());
	}

	//render the scene and camera since there are layer coding will be here
	// to draw the scene and  (hud / ui)
	render() {
		super.render(); //callback to render it

		if(this.controls !=null){
			this.controls.update();
			//console.log("control update...");
		}
		//console.log(this);
		//this.renderer.setClearColor( 0xffffff, 1 );
		this.renderer.clear();
		if (this.bdisplayscene == true){
			  this.renderer.render(this.scene, this.camera);
		}
		this.renderer.clearDepth();
		//console.log("testing...");
		if(this.world != null){
			  this.world.step(this.timeStep);
			  //console.log(this.world);
			  //console.log(this.timeStep);
		}

		for(var i = 0; i < this.objectphysics.length; i++){
			this.objectphysics[i].position.copy(this.objectphysics[i].shape.position);
			//console.log(this.objectphysics[i].update);
			if(this.objectphysics[i].update){
				this.objectphysics[i].update();
			}

		}

		/*
		for(i = 0; i < this.objectphysics.length; i++){
			  this.objectphysics[i].position.set(
					this.objectphysics[i].shape.position.x,
					this.objectphysics[i].shape.position.y,
					this.objectphysics[i].shape.position.z
			  );
		}
		*/
		//this.objectphysics.push(player);
		//console.log(this.objectphysics.length);
	}


	resizeHandler() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		//console.log(this.camera);
		this.camera.updateProjectionMatrix();
		this.renderer.setSize(window.innerWidth, window.innerHeight);
		this.controls.handleResize();
	}
	// cannon
	setup_ground(){
		// Create a plane
		var groundShape = new CANNON.Plane();
		var groundBody = new CANNON.RigidBody(0,groundShape);
		groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0),-Math.PI/2);
		this.world.add(groundBody);
		//this.create_physictest();
		//createphysics_object(this.scene,this.world,this.objectphysics);

	}

	create_physictest(){
		console.log("add scene object...");
		var geometry = new THREE.BoxGeometry(1,1,1);
		var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
		var player = new THREE.Mesh( geometry, material );
		player.position.set(0,3,0);

		var mass = 5, radius = 1;
		var sphereShape = new CANNON.Sphere(radius);
		var sphereBody = new CANNON.RigidBody(mass, sphereShape);

		player.shape = sphereBody;

		sphereBody.position.set(0,10,0);
		this.world.add(sphereBody);
		this.scene.add( player );
		this.objectphysics.push(player);
		//objectphysics
		//console.log(player.shape);
		//console.log(this.objectphysics.length);
		//console.log(this.objectphysics.length);
	}

	init_pointer(){
		console.log("init no mouse pointer");//hide mouse

		var _document:any = document;//this will try to remove error from typescript
		//var blocker = _document.getElementById( 'blocker' );
		//var instructions = _document.getElementById( 'instructions' );
		var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

		function reqestpointer(){
			  element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
			  if ( /Firefox/i.test( navigator.userAgent ) ) {
					var fullscreenchange = function ( event ) {
						if ( _document.fullscreenElement === element || _document.mozFullscreenElement === element || _document.mozFullScreenElement === element ) {
							_document.removeEventListener( 'fullscreenchange', fullscreenchange );
							_document.removeEventListener( 'mozfullscreenchange', fullscreenchange );
							element.requestPointerLock();
						}
					}
					document.addEventListener( 'fullscreenchange', fullscreenchange, false );
					document.addEventListener( 'mozfullscreenchange', fullscreenchange, false );
					element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
					element.requestFullscreen();
			  } else {
					element.requestPointerLock();
					console.log("click requestPointerLock");
			  }
		}

		var idfocus = document.getElementById('focus');
		idfocus.addEventListener("click",function(e){
			reqestpointer();
		}, false);

		//document.addEventListener("keydown", doKeyDown, false);
		document.addEventListener("keyup", doKeyDown, false);
		//window.addEventListener( "keypress", doKeyDown, false );
		function doKeyDown(e) {
			//console.log( e.keyCode );
			if(e.keyCode == 220){
				reqestpointer();
			}
		}

		if ( havePointerLock ) {
			var element = _document.body; //this will try to remove error from typescript
			var pointerlockchange = function ( event ) {
				if ( _document.pointerLockElement === element || _document.mozPointerLockElement === element || _document.webkitPointerLockElement === element ) {
					  //fpscontrol.enabled = true;
					  //blocker.style.display = 'none';
					  //console.log(" fpscontrol true");
				} else {
					  //fpscontrol.enabled = false;
					  console.log(" fpscontrol false");
					  //blocker.style.display = '-webkit-box';
					  //blocker.style.display = '-moz-box';
					  //blocker.style.display = 'box';
					  //instructions.style.display = '';
				}
			}
			var pointerlockerror = function ( event ) {
				//instructions.style.display = '';
			}
			// Hook pointer lock state change events
			document.addEventListener( 'pointerlockchange', pointerlockchange, false );
			document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
			document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );

			document.addEventListener( 'pointerlockerror', pointerlockerror, false );
			document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
			document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );

			//instructions.addEventListener( 'click', function ( event ) {
				//console.log("click CannonPointerLockControls");
				//instructions.style.display = 'none';
				// Ask the browser to lock the pointer
				//reqestpointer();
			// }, false );
		} else {
			  //instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';
		}

	}

	//===================================================================
	//player handlers section
	//===================================================================

	CreatePlayerObject(data:any){
		var geometry = new THREE.BoxGeometry(1,1,1);
		var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
		var player = new THREE.Mesh( geometry, material );
		player.userid = data['userid'];
		player.name = "playercube";
		this.scene.add(player);
		this.currentplayer = player;
	}

}

//init game
var game = new ThreejsGame();



//console.log(game);
