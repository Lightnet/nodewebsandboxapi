/*
    Project Name: Node Web Sandbox API
    Link:https://bitbucket.org/Lightnet/nodewebsandboxapi
    Created By: Lightnet
    License: Please read the readme.txt file for more information.
  
    Information:
    
*/
//declare var mongoose;
//var mongooseclass = require('mongoose');
//module.exports = mongoose = mongooseclass.createConnection(config.database);
//mongoose = require('mongoose');
var config = require(__dirname + "/../config.js");
var mongoose = require('mongoose');
mongoose.connect(config.database);
