/*
      Project Name: Node Web Sandbox
      Link:https://bitbucket.org/Lightnet/nodewebsandbox

      Information: To create a bot and rpg system with the module plug-in build prototype.
      Currently work in progress builds.

*/
declare var THREE;
declare var Detector;
declare var CANNON;
// <reference path="../DefinitelyTyped/threejs/three.d.ts" />

class ThreejsBase {
	static self:any;

	isClient:boolean = true;
	isServer:boolean = false;
	isNetwork:boolean = false;//make sure it not network else it go into that ...

	//cannon.js
	world:any;
	boxbody:any;
	timeStep:any = 1/60;

	id:string;
	scene:any;
	camera:any;
	renderer:any;

	WIDTH:number = window.innerWidth;
	HEIGHT:number = window.innerHeight;
	scenes:any = [];
	renders:any = [];
	worlds:any = [];
	//https://github.com/jeromeetienne/threex.colliders
	colliderSystem:any  = new THREEx.ColliderSystem();


	//build the simple layer
	constructor(){
		//wait for window to load to find the id to set it.
		window.addEventListener('load',()=> this.setup());
    }

    setup() {
		this.init();
		this.initCannon();
		//console.log(this);
		this.render();
		//this.inittest();
    }

	init(){
		console.log('init three js engine...');
		this.scene = new THREE.Scene();
		this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000000 );
		this.camera.position.z = 10;
		console.log('done init...');
		var centervector = new THREE.Vector3(0,0,0);
		this.camera.lookAt( centervector );

		if ( Detector.webgl ){
			  this.renderer = new THREE.WebGLRenderer( {antialias:true} );
		}else{
			  //renderer = new THREE.CanvasRenderer({ canvas: layer3D.getCanvas().getElement() });
		  this.renderer = new THREE.CanvasRenderer();
		}

		//this.renderer = new THREE.WebGLRenderer( {antialias:true} );

		this.renderer.setSize( this.WIDTH, this.HEIGHT);
		this.renderer.setClearColor( 0xffffff, 1 );
		this.renderer.autoClear = false; // To allow render overlay on top of sprited sphere
		this.renderer.domElement.id = "canvas";//set id to make sure fix for scroll

		THREEx.WindowResize(this.renderer, this.camera);//resize window borders and camera ratio

		var _render = document.getElementById('render');//get id from div
		_render.appendChild( this.renderer.domElement );
	}

	initCannon(){
		this.world = new CANNON.World();
		this.world.gravity.set(0,-20,0);
		this.world.quatNormalizeSkip = 0;
		this.world.quatNormalizeFast = false;
		var solver = new CANNON.GSSolver();
		this.world.defaultContactMaterial.contactEquationStiffness = 1e9;
		this.world.defaultContactMaterial.contactEquationRegularizationTime = 4;
		solver.iterations = 7;
		solver.tolerance = 0.1;
		var split = true;
		if(split){
			this.world.solver = new CANNON.SplitSolver(solver);
		}else{
			this.world.solver = solver;
		}
		this.world.broadphase = new CANNON.NaiveBroadphase();
	}
	//http://stackoverflow.com/questions/21924719/how-to-use-requestanimationframe-with-a-typescript-object
	// render the scene;
	//public render=()=> {
	render() {
		//requestAnimationFrame(this.render);
		//requestAnimationFrame(()=> {this.render()}); //works
		requestAnimationFrame(()=> this.render()); //works
		//console.log(this);
		//console.log("update...");

		//===
		//works
		//this.renderer.render(this.scene, this.camera);
		//this.renderer.clearDepth();
		//if(this.world != null){
			  //this.world.step(this.timeStep);
		// }
		//===
	}

	inittest(){
		var axes = new THREE.AxisHelper(500);
		this.scene.add(axes);
	}
}

//var threeengine = new ThreejsEngine();
//console.log(threeengine);
