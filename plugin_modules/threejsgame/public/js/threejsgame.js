/*
    Project Name: Node Web Sandbox API
    Link:https://bitbucket.org/Lightnet/nodewebsandboxapi
    Created By: Lightnet
    License: Please read the readme.txt file for more information.
  
    Information:
    
*/
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="./threejsbase.ts" />
//declare var Math;
//declare var Window:any;
var ThreejsGame = (function (_super) {
    __extends(ThreejsGame, _super);
    //}
    function ThreejsGame() {
        _super.call(this);
        this.bdisplayscene = true;
        this.objectphysics = [];
        this.sceneobjects = [];
        this.control_x = 0;
        this.control_y = 0;
        this.up = false;
        this.down = false;
        this.right = false;
        this.left = false;
        this.bplayercontrol = true;
        this.traceobjects = [];
        this.Mouse = new THREE.Vector2();
        this.raycaster = new THREE.Raycaster();
        this.selectIDHash = "";
        this.selectObjects = [];
        this.projector = new THREE.Projector();
        this.bBuildMode = false;
        this.bBuildPress = false;
        this.bSnap = false;
        this.gridSize = 64;
        this.count = 0;
        console.log("init game");
    }
    ThreejsGame.prototype.self = function () {
        return this;
    };
    //build objects filter
    ThreejsGame.prototype.CreateBuildType = function () {
        //this.mouseobject.position
        console.log("build...");
        var geometry = new THREE.BoxGeometry(32, 32, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var newcube = new THREE.Mesh(geometry, material);
        //newcube.userid = data['userid'];
        newcube.name = "cube";
        newcube.position.set(this.mouseobject.position.x, this.mouseobject.position.y, this.mouseobject.position.z);
        newcube.idhash = "obj" + md5(newcube.uuid);
        this.scene.add(newcube);
        this.sceneobjects.push(newcube);
    };
    ThreejsGame.prototype.SimpleCubeSaveTest = function () {
        //this.mouseobject.position
        console.log("build...");
        var geometry = new THREE.BoxGeometry(32, 32, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var newcube = new THREE.Mesh(geometry, material);
        //newcube.userid = data['userid'];
        newcube.name = "cube";
        newcube.idhash = "obj" + md5(newcube.uuid); //ID system is need for server side
        newcube.position.set(0, 0, 0);
        this.scene.add(newcube);
        this.sceneobjects.push(newcube);
    };
    //toggle build mode
    ThreejsGame.prototype.ToggleBuild = function () {
        if (this.bBuildMode) {
            this.bBuildMode = false;
        }
        else {
            this.bBuildMode = true;
        }
        console.log("bBuildMode:" + this.bBuildMode);
    };
    //snap object
    ThreejsGame.prototype.ToggleSnap = function () {
        if (this.bSnap) {
            this.bSnap = false;
        }
        else {
            this.bSnap = true;
        }
        console.log("bSnap:" + this.bSnap);
    };
    //set size grid
    ThreejsGame.prototype.setGridSize = function (_size) {
        if (_size == 16) {
            this.gridSize = _size;
        }
        if (_size == 32) {
            this.gridSize = _size;
        }
        if (_size == 64) {
            this.gridSize = _size;
        }
        console.log("_size:" + _size);
    };
    ThreejsGame.prototype.SaveSceneObjects = function () {
        console.log(socket);
        console.log("Scene List:");
        for (var i = 0; i < this.sceneobjects.length; i++) {
            console.log(this.sceneobjects[i]);
            console.log(this.sceneobjects[i].name);
            var objectarray = {
                action: 'savesceneobject',
                type: 'mesh',
                idname: this.sceneobjects[i].name,
                //idhash: "obj"+ md5( this.sceneobjects[i].uuid ),
                idhash: this.sceneobjects[i].idhash,
                name: this.sceneobjects[i].name,
                position: [this.sceneobjects[i].position.x, this.sceneobjects[i].position.y, this.sceneobjects[i].position.z],
                rotation: [this.sceneobjects[i].rotation.x, this.sceneobjects[i].rotation.y, this.sceneobjects[i].rotation.z],
                scale: [this.sceneobjects[i].scale.x, this.sceneobjects[i].scale.y, this.sceneobjects[i].scale.z],
                params: {}
            };
            socket.emit('threejs', objectarray);
        }
    };
    ThreejsGame.prototype.isEmpty = function (str) {
        return (!str || 0 === str.length);
    };
    ThreejsGame.prototype.DeleteSceneObject = function () {
        console.log("DeleteSceneObject:" + this.selectIDHash);
        var _id = this.selectIDHash;
        if (!this.isEmpty(_id)) {
            socket.emit('threejs', { action: 'deletesceneobject', idhash: _id });
        }
        //this.selectIDHash = "";
    };
    ThreejsGame.prototype.LoadSceneObjects = function () {
        console.log("LoadSceneObjects:");
        socket.emit('threejs', { action: 'loadsceneobjects' });
    };
    ThreejsGame.prototype.ClearSceneObjects = function () {
        console.log("remove objects:");
        socket.emit('threejs', { action: 'clearsceneobjects' });
    };
    ThreejsGame.prototype.ClearSceneObjectsLocal = function () {
        console.log("ClearSceneObjectsLocal:");
        for (var i = 0; i < this.sceneobjects.length; i++) {
            this.scene.remove(this.sceneobjects[i]);
        }
        this.sceneobjects = [];
    };
    //testing box
    ThreejsGame.prototype.buildgeom = function () {
        var geometry = new THREE.BoxGeometry(200, 150, 50);
        geometry.faces[0].color.setHex(0x890202);
        geometry.faces[1].color.setHex(0x890202);
        geometry.faces[2].color.setHex(0x9e0208);
        geometry.faces[3].color.setHex(0x9e0208);
        geometry.faces[4].color.setHex(0xFF0000);
        geometry.faces[5].color.setHex(0x333333);
        var material = new THREE.MeshBasicMaterial({ vertexColors: THREE.FaceColors });
        var side = 1;
        var object = new THREE.Mesh(geometry, material);
        object.position.x = 0;
        object.position.y = -5;
        object.position.z = 0;
        //object.position.x = Math.cos(side * Math.PI/2) * 200;
        //object.position.z = -Math.sin(side * Math.PI/2) * 200;
        //object.rotation.y = (side+1) * Math.PI/2;
        this.scene.add(object);
    };
    //text3d:any;
    // this will trigger event on load listener
    // put setup code here
    ThreejsGame.prototype.setup = function () {
        var _this = this;
        _super.prototype.setup.call(this);
        this.manager = new THREE.LoadingManager();
        this.manager.onProgress = function (item, loaded, total) {
            console.log(item, loaded, total);
        };
        //this.init_pointer();
        this.renderer.setClearColor(0xffffff, 1);
        this.renderer.autoClear = false;
        this.setup_ground();
        //this.camera.position.set()
        this.camera.position.z = 300;
        this.camera.position.y = 300;
        var centervector = new THREE.Vector3(0, 0, 0);
        this.camera.lookAt(centervector);
        console.log("setup here...");
        var axes = new THREE.AxisHelper(500);
        this.scene.add(axes);
        this.SetupOrthographicScene();
        //this.buildgeom();
        //==========
        //mouse
        //==========
        this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
        this.controls.handleResize();
        //this.renderer.domElement.addEventListener('mousedown', this.onDocumentMouseDown, false);
        this.renderer.domElement.addEventListener('mousedown', MouseDown, false);
        //this.renderer.domElement.addEventListener('mouseup', this.onDocumentMouseUp, false);
        this.renderer.domElement.addEventListener('mouseup', MouseUp, false);
        this.renderer.domElement.addEventListener('mousemove', MouseMove, false);
        this.renderer.domElement.addEventListener('mouseout', MouseOut, false);
        var self = this;
        function MouseDown(event) {
            console.log("down");
            self.onDocumentMouseDown(event, self);
        }
        function MouseUp(event) {
            self.onDocumentMouseUp(event, self);
        }
        function MouseMove(event) {
            self.onDocumentMouseMove(event, self);
        }
        function MouseOut(event) {
            self.onDocumentMouseOut(event, self);
        }
        //console.log(this.controls);
        // RESIZE
        window.addEventListener('resize', function () { return _this.resizeHandler(); }, false);
        document.addEventListener('keydown', KeyBoardDown, false);
        document.addEventListener('keyup', KeyBoardUp, false);
        function KeyBoardUp(event) {
            self.KeyUpControl(event, self);
            //console.log( ()=> this.outputprint() );
            //()=> this.outputprint();
        }
        function KeyBoardDown(event) {
            self.KeyDownControl(event, self);
        }
        if (socket != null) {
            console.log("Socket not null.");
            socket.on('threejs', function (data) {
                console.log("incoming threejs data...");
                if (data['action'] != null) {
                    if (data['action'] == 'addsceneobject') {
                        console.log("add cube");
                        self.AddSceneObjectIO(data);
                    }
                }
            });
        }
        else {
            console.log("Socket null!");
        }
        //createplayer_object(this.scene, this.world,this.objectphysics);
        //this.loadobj();
        this.create_cube();
        //this.SimpleCubeSaveTest();
        //this.Create2DTest();
        //this.CreateDivInfo();
        this.CreateParticleTest();
        //console.log("text3d");
        //console.log(text3d);
        this.CreateObjectDisplayText();
    };
    ThreejsGame.prototype.CreateObjectDisplayText = function () {
        var width = window.innerWidth / 2;
        var height = window.innerHeight / 2;
        var imageWidth = 0;
        var imageHeight = 0;
        this.objecttextid = this.Create2DText(this.sceneOrtho, { text: "ID Hash:000000000000000000000000000000000", fontsize: 12 });
        imageWidth = this.objecttextid['material'].map.image.width / 2;
        imageHeight = this.objecttextid['material'].map.image.height / 2;
        console.log(imageWidth + ":" + imageWidth);
        this.objecttextid['sprite'].position.set(-width + imageWidth, height - imageHeight - 28 * 1, 1);
        //this.objecttextid['sprite'].position.set(0,   height - imageHeight - 28*0, 1 );
        this.objecttextposition = this.Create2DText(this.sceneOrtho, { text: "POS:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", fontsize: 12 });
        imageWidth = this.objecttextposition['material'].map.image.width / 2;
        imageHeight = this.objecttextposition['material'].map.image.height / 2;
        this.objecttextposition['sprite'].position.set(-width + imageWidth, height - imageHeight - 28 * 2, 1);
        //this.objecttextposition['sprite'].position.set(0,   height - imageHeight - 28*1, 1 );
        this.objecttextrotation = this.Create2DText(this.sceneOrtho, { text: "ROT:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", fontsize: 12 });
        imageWidth = this.objecttextrotation['material'].map.image.width / 2;
        imageHeight = this.objecttextrotation['material'].map.image.height / 2;
        this.objecttextrotation['sprite'].position.set(-width + imageWidth, height - imageHeight - 28 * 3, 1);
        //this.objecttextrotation['sprite'].position.set(0,   height - imageHeight - 28*2, 1 );
        this.objecttextscale = this.Create2DText(this.sceneOrtho, { text: "Scale:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", fontsize: 12 });
        imageWidth = this.objecttextscale['material'].map.image.width / 2;
        imageHeight = this.objecttextscale['material'].map.image.height / 2;
        this.objecttextscale['sprite'].position.set(-width + imageWidth, height - imageHeight - 28 * 4, 1);
        //this.objecttextscale['sprite'].position.set(0,   height - imageHeight - 28*3, 1 );
    };
    ThreejsGame.prototype.SetupOrthographicScene = function () {
        var width = window.innerWidth;
        var height = window.innerHeight;
        var spriteTL, spriteTR, spriteBL, spriteBR, spriteC;
        var self = this;
        this.cameraOrtho = new THREE.OrthographicCamera(-width / 2, width / 2, height / 2, -height / 2, 1, 10);
        this.cameraOrtho.position.z = 10;
        this.sceneOrtho = new THREE.Scene();
        //var mapA = THREE.ImageUtils.loadTexture( "textures/sprite0.png", undefined, createHUDSprites );
        var loader = new THREE.TextureLoader();
        var self = this;
        loader.load(
        // resource URL
        'textures/sprite0.png', 
        // Function when resource is loaded
        function (texture) {
            createHUDSprites(texture);
            //console.log(THREE.Sprite);
            //var ballMaterial = new THREE.SpriteMaterial( { map: texture, useScreenCoordinates: true, alignment: THREE.SpriteAlignment.topLeft  } );
            //var ballMaterial = new THREE.SpriteMaterial( { map: texture } );
            //var sprite = new THREE.Sprite( ballMaterial );
            /*
            // do something with the texture
            var material = new THREE.MeshBasicMaterial( {
                map: texture
             });
             
            var sprite = new THREE.Sprite( material );
            self.scene.add( sprite );
            console.log(sprite);
            */
        }, 
        // Function called when download progresses
        function (xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');
        }, 
        // Function called when download errors
        function (xhr) {
            console.log('An error happened');
        });
        function createHUDSprites(texture) {
            var material = new THREE.SpriteMaterial({ map: texture });
            var width = material.map.image.width;
            var height = material.map.image.height;
            //console.log(width+":"+height);
            spriteTL = new THREE.Sprite(material);
            spriteTL.scale.set(width, height, 1);
            //self.sceneOrtho.add( spriteTL );
            //spriteTR = new THREE.Sprite( material );
            //spriteTR.scale.set( width, height, 1 );
            //self.sceneOrtho.add( spriteTR );
            //spriteBL = new THREE.Sprite( material );
            //spriteBL.scale.set( width, height, 1 );
            //self.sceneOrtho.add( spriteBL );
            //spriteBR = new THREE.Sprite( material );
            //spriteBR.scale.set( width, height, 1 );
            //self.sceneOrtho.add( spriteBR );
            //spriteC = new THREE.Sprite( material );
            //spriteC.scale.set( width, height, 1 );
            //self.sceneOrtho.add( spriteC );
            updateHUDSprites();
        }
        function updateHUDSprites() {
            var width = window.innerWidth / 2;
            var height = window.innerHeight / 2;
            var material = spriteTL.material;
            var imageWidth = material.map.image.width / 2;
            var imageHeight = material.map.image.height / 2;
            //spriteTL.position.set( - width + imageWidth,   height - imageHeight, 1 ); // top left
            //spriteTR.position.set(   width - imageWidth,   height - imageHeight, 1 ); // top right
            //spriteBL.position.set( - width + imageWidth, - height + imageHeight, 1 ); // bottom left
            //spriteBR.position.set(   width - imageWidth, - height + imageHeight, 1 ); // bottom right
            //spriteC.position.set( 0, 0, 1 ); // center
        }
    };
    ThreejsGame.prototype.CreateDivInfo = function () {
        var container = document.createElement('div');
        document.body.appendChild(container);
        var info = document.createElement('div');
        info.style.position = 'absolute';
        info.style.top = '10px';
        info.style.width = '100%';
        info.style.textAlign = 'center';
        info.innerHTML = '<a href="http://threejs.org" target="_blank">three.js</a> Webgl';
        container.appendChild(info);
    };
    ThreejsGame.prototype.CreateParticleTest = function () {
        /*
        var PI2 = Math.PI * 2;
        
        var programStroke = function ( context ) {
            context.lineWidth = 0.025;
            context.beginPath();
            context.arc( 0, 0, 0.5, 0, PI2, true );
            context.stroke();
        };
        
        var particle = new THREE.Sprite( new THREE.SpriteCanvasMaterial( { color: Math.random() * 0x808080 + 0x808080, program: programStroke } ) );
        particle.scale.x = particle.scale.y = Math.random() * 20 + 20;
        
        this.scene.add( particle );
        */
    };
    //sprite_texture:any;
    //sprite_material:any;
    ThreejsGame.prototype.Create2DTest = function () {
        var rendererType = "3d";
        var canvas = document.createElement('canvas'), context = canvas.getContext('2d'), metrics = null, textHeight = 100, textWidth = 0, actualFontSize = 12;
        context.font = "normal " + textHeight + "px Arial";
        metrics = context.measureText("Sample Text in " + rendererType);
        textWidth = metrics.width;
        canvas.width = textWidth;
        canvas.height = textHeight;
        context.font = "normal " + textHeight + "px Arial";
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillStyle = "#ff0000";
        context.fillText("Sample Text in " + rendererType, textWidth / 2, textHeight / 2);
        var texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        //var material = new THREE.SpriteMaterial({ map: texture, useScreenCoordinates: false, alignment: THREE.SpriteAlignment.center });
        var material = new THREE.SpriteMaterial({ map: texture });
        material.transparent = true;
        //var textObject = new THREE.Sprite(material);
        var textObject = new THREE.Object3D();
        var sprite = new THREE.Sprite(material);
        textObject.textHeight = actualFontSize;
        textObject.textWidth = (textWidth / textHeight) * textObject.textHeight;
        if (rendererType == "2d") {
            sprite.scale.set(textObject.textWidth / textWidth, textObject.textHeight / textHeight, 1);
        }
        else {
            sprite.scale.set(textWidth / textHeight * actualFontSize, actualFontSize, 1);
        }
        //textObject.add(sprite);
        //this.sceneOrtho.add(textObject);
        this.sceneOrtho.add(sprite);
        //var text3d = this.Create2DText(this.sceneOrtho,"hello",12);
        //console.log("text3d");
        //console.log(text3d);
    };
    //3d in sprite text update...
    ThreejsGame.prototype.Create2DText = function (_scene, options) {
        if (options === void 0) { options = {}; }
        var text3D = [];
        text3D['metrics'] = null;
        text3D['textHeight'] = 64;
        text3D['textWidth'] = 0;
        text3D['rendererType'] = "3d";
        text3D['canvas'] = document.createElement('canvas');
        if (options['text'] != null) {
            text3D['text'] = options['text'];
        }
        else {
            options['text'] = "none";
        }
        if (options['fontsize'] != null) {
            text3D['actualFontSize'] = options['fontsize'];
        }
        else {
            text3D['actualFontSize'] = 12;
        }
        //text3D['actualFontSize'] = 12;
        text3D['context'] = text3D['canvas'].getContext('2d');
        text3D['drawtext'] = function (__text) {
            var context = text3D['context'];
            context.font = "normal " + text3D['actualFontSize'] + "px Arial";
            //text3D['metrics'] = context.measureText("Sample Text in " + text3D['rendererType']);
            text3D['metrics'] = context.measureText(__text);
            //console.log('metrics');
            //console.log(text3D['metrics']);
            text3D['textWidth'] = text3D['metrics'].width;
            console.log(text3D['textWidth']);
            //text3D['canvas'].width = text3D['textWidth'];
            //text3D['canvas'].height = text3D['textHeight'];
            text3D['canvas'].width = 512;
            text3D['canvas'].height = 128;
            context.font = "normal " + text3D['actualFontSize'] + "px Arial";
            //context.textAlign = "center";
            //context.textAlign = "center";
            context.textBaseline = "middle";
            //context.textBaseline = "alphabetic";
            //context.fillStyle ="#dbbd7a";
            //context.fill();
            //context.fillStyle = "#ff0000";
            //context.fillText(__text , text3D['textWidth'] / 2, text3D['textHeight'] / 2);
            context.fillStyle = "#000000";
            context.fillText(__text, 0, text3D['textHeight'] / 2);
            //context.fill();
            //context.fillStyle = "#ff0000";
            //context.strokeStyle="#FF0000";
            //context.rect(0,0,text3D['canvas'].width,text3D['canvas'].height);
            context.stroke();
            //context.fill();
            text3D['context'] = context;
            if (text3D['sprite'] != null) {
                //update canvas to sprite
                text3D['texture'].image = text3D['canvas'];
                text3D['texture'].needsUpdate = true;
                console.log(text3D['texture']);
            }
        };
        text3D['drawtext'](options['text']);
        text3D['texture'] = new THREE.Texture(text3D['canvas']);
        text3D['texture'].needsUpdate = true;
        console.log(text3D['texture']);
        //text3D['texture'] = new THREE.Texture();
        //console.log(text3D['texture']);
        text3D['material'] = new THREE.SpriteMaterial({ map: text3D['texture'] });
        text3D['material'].transparent = true;
        console.log(text3D['material']);
        //var textObject = new THREE.Sprite(material);
        //var textObject = new THREE.Object3D();
        text3D['sprite'] = new THREE.Sprite(text3D['material']);
        //console.log(text3D['sprite']);
        //textObject.textHeight = actualFontSize;
        //textObject.textWidth = (textWidth / textHeight) * textObject.textHeight;
        if (text3D['rendererType'] == "2d") {
        }
        else {
            text3D['sprite'].scale.set(384, 128, 1);
        }
        //textObject.add(sprite);
        //this.sceneOrtho.add(textObject);
        //this.sceneOrtho.add(sprite);
        _scene.add(text3D['sprite']);
        return text3D;
    };
    //check object type for adding.
    ThreejsGame.prototype.AddSceneObjectIO = function (_data) {
        console.log("data object...");
        console.log(_data);
        if (_data['idname'] == 'cube') {
            console.log("found cube!");
            this.CreateCube(_data);
        }
    };
    //create params cube
    ThreejsGame.prototype.CreateCube = function (_data) {
        var bfound = false;
        for (var i = 0; i < this.sceneobjects.length; i++) {
            if (this.sceneobjects[i].idhash == _data['idhash']) {
                bfound = true;
                console.log("FOUND!");
                break;
            }
        }
        if (bfound == false) {
            //console.log("ADD!");
            var geometry = new THREE.BoxGeometry(32, 32, 32);
            var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
            var newcube = new THREE.Mesh(geometry, material);
            newcube.position.set(_data['position'][0], _data['position'][1], _data['position'][2]);
            newcube.rotation.set(_data['rotation'][0], _data['rotation'][1], _data['rotation'][2]);
            newcube.scale.set(_data['scale'][0], _data['scale'][1], _data['scale'][2]);
            newcube.idhash = _data['idhash'];
            newcube.idname = _data['idname'];
            newcube.name = _data['name'];
            this.scene.add(newcube);
            this.sceneobjects.push(newcube);
        }
    };
    //ThreejsGame.prototype.outputprint();
    ThreejsGame.prototype.outputprint = function () {
        console.log("hello");
    };
    ThreejsGame.prototype.create_cube = function () {
        console.log("add scene object...");
        var geometry = new THREE.BoxGeometry(32, 32, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var cubeplace = new THREE.Mesh(geometry, material);
        cubeplace.position.set(0, 3, 0);
        this.scene.add(cubeplace);
        this.mouseobject = cubeplace;
    };
    ThreejsGame.prototype.loadobj = function () {
        var self = this;
        var loader = new THREE.OBJLoader(this.manager);
        loader.load('assets/models/building_space_blocks.obj', function (object) {
            object.traverse(function (child) {
                if (child instanceof THREE.Mesh) {
                }
            });
            //object.position.y = - 80;
            self.scene.add(object);
        }, self.onProgress, self.onError);
    };
    ThreejsGame.prototype.onProgress = function (xhr) {
        if (xhr.lengthComputable) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            //percentComplete = Math.round(percentComplete, 2);
            console.log(percentComplete + '% downloaded');
        }
    };
    ThreejsGame.prototype.onError = function (xhr) {
    };
    //87 = up
    //83 = down
    //65 = left
    //68 = right
    //
    ThreejsGame.prototype.KeyDownControl = function (event, _self) {
        ThreejsGame.prototype.outputprint();
        //console.log('test');
        //console.log( event.keyCode );
        if (event.keyCode == 66) {
            if (this.bBuildPress == false) {
                this.bBuildPress = true;
                _self.CreateBuildType();
            }
        }
    };
    ThreejsGame.prototype.KeyUpControl = function (event, _self) {
        //console.log( "UP" );
        //console.log( event.keyCode );
        if (event.keyCode == 66) {
            this.bBuildPress = false;
        }
    };
    ThreejsGame.prototype.showlog = function () {
        console.log("test");
    };
    ThreejsGame.prototype.onDocumentMouseDown = function (event, _self) {
        event.preventDefault();
        //console.log(this);
        this.raycaster.setFromCamera(this.Mouse, this.camera);
        var intersects = this.raycaster.intersectObjects(this.sceneobjects);
        if (intersects.length > 0) {
            if (this.INTERSECTED != intersects[0].object) {
                this.selectObject = intersects[0].object;
                this.selectIDHash = intersects[0].object.idhash;
                //console.log(intersects[ 0 ].object.position);
                //console.log(intersects[ 0 ].object);
                this.SelectSceneObject(intersects[0].object);
            }
        }
        /*
        var intersects = this.raycaster.intersectObjects(this.scene.children );
        if ( intersects.length > 0 ) {
            if ( this.INTERSECTED != intersects[ 0 ].object ) {
                //console.log(intersects[ 0 ].object.position);
                console.log(intersects[ 0 ].object);
            }
        }
        */
        console.log("onDocumentMouseDown");
    };
    ThreejsGame.prototype.onDocumentMouseUp = function (event, _self) {
        event.preventDefault();
        console.log("onDocumentMouseUp");
    };
    ThreejsGame.prototype.onDocumentMouseOut = function (event, _self) {
        event.preventDefault();
    };
    ThreejsGame.prototype.onDocumentMouseMove = function (event, _self) {
        event.preventDefault();
        //console.log(this);
        var self = this;
        var x, y;
        if (self.Mouse != null) {
            self.Mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
            self.Mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
        }
        //console.log("move"+self.Mouse.x + ":" + self.Mouse.y);
        //console.log("move"+self());
        //console.log(this.self());
        var projector = new THREE.Projector();
        var planeZ = new THREE.Plane(new THREE.Vector3(0, 1, 0), 0); //x=0,y=1,z=0(note they this work.)
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        //projector.unprojectVector( vector, camera );
        vector.unproject(this.camera);
        var raycaster = new THREE.Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());
        var pos = raycaster.ray.intersectPlane(planeZ);
        //console.log("x: " + pos.x + ", y: " + pos.y + ',z:'+pos.z);
        var newpos;
        if (this.bSnap) {
            newpos = new THREE.Vector3(Math.floor(pos.x / this.gridSize) * this.gridSize, Math.floor(pos.y / this.gridSize) * this.gridSize, Math.floor(pos.z / this.gridSize) * this.gridSize);
        }
        else {
            if (pos != null) {
                newpos = new THREE.Vector3(pos.x, pos.y, pos.z);
            }
        }
        if (newpos != null) {
            this.mouseobject.position.set(newpos.x, newpos.y, newpos.z);
        }
    };
    ThreejsGame.prototype.SelectSceneObject = function (_object) {
        //text3D['drawtext'](options['text']);
        if (_object != null) {
            if (this.objecttextid != null) {
                this.objecttextid['drawtext']("ID Hash:" + _object.idhash);
            }
            if (this.objecttextposition != null) {
                this.objecttextposition['drawtext']("POS x:" + _object.position.x + ":y" + _object.position.y + ":z" + _object.position.z);
            }
            if (this.objecttextrotation != null) {
                this.objecttextrotation['drawtext']("ROT x:" + _object.rotation.x + ":y" + _object.rotation.y + ":z" + _object.rotation.z);
            }
            if (this.objecttextscale != null) {
                this.objecttextscale['drawtext']("SCALE x:" + _object.scale.x + ":y" + _object.scale.y + ":z" + _object.scale.z);
            }
        }
    };
    //render the scene and camera since there are layer coding will be here
    // to draw the scene and  (hud / ui)
    ThreejsGame.prototype.render = function () {
        _super.prototype.render.call(this); //callback to render it
        //this.count += 1;
        //console.log(this.count);
        //if(this.text3d !=null){
        //this.text3d['drawtext'](this.count);
        //}
        if (this.controls != null) {
            this.controls.update();
        }
        //console.log(this);
        //this.renderer.setClearColor( 0xffffff, 1 );
        /*
        this.renderer.clear();
        if (this.bdisplayscene == true){
              this.renderer.render(this.scene, this.camera);
        }
        this.renderer.clearDepth();
        */
        this.renderer.clear();
        this.renderer.render(this.scene, this.camera);
        this.renderer.clearDepth();
        if ((this.sceneOrtho != null) || (this.cameraOrtho != null)) {
            this.renderer.render(this.sceneOrtho, this.cameraOrtho);
        }
        //console.log("testing...");
        if (this.world != null) {
            this.world.step(this.timeStep);
        }
        for (var i = 0; i < this.objectphysics.length; i++) {
            this.objectphysics[i].position.copy(this.objectphysics[i].shape.position);
            //console.log(this.objectphysics[i].update);
            if (this.objectphysics[i].update) {
                this.objectphysics[i].update();
            }
        }
        /*
        for(i = 0; i < this.objectphysics.length; i++){
              this.objectphysics[i].position.set(
                    this.objectphysics[i].shape.position.x,
                    this.objectphysics[i].shape.position.y,
                    this.objectphysics[i].shape.position.z
              );
        }
        */
        //this.objectphysics.push(player);
        //console.log(this.objectphysics.length);
    };
    ThreejsGame.prototype.resizeHandler = function () {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        //console.log(this.camera);
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.controls.handleResize();
    };
    // cannon
    ThreejsGame.prototype.setup_ground = function () {
        // Create a plane
        var groundShape = new CANNON.Plane();
        var groundBody = new CANNON.RigidBody(0, groundShape);
        groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);
        this.world.add(groundBody);
        //this.create_physictest();
        //createphysics_object(this.scene,this.world,this.objectphysics);
    };
    ThreejsGame.prototype.create_physictest = function () {
        console.log("add scene object...");
        var geometry = new THREE.BoxGeometry(1, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var player = new THREE.Mesh(geometry, material);
        player.position.set(0, 3, 0);
        var mass = 5, radius = 1;
        var sphereShape = new CANNON.Sphere(radius);
        var sphereBody = new CANNON.RigidBody(mass, sphereShape);
        player.shape = sphereBody;
        sphereBody.position.set(0, 10, 0);
        this.world.add(sphereBody);
        this.scene.add(player);
        this.objectphysics.push(player);
        //objectphysics
        //console.log(player.shape);
        //console.log(this.objectphysics.length);
        //console.log(this.objectphysics.length);
    };
    ThreejsGame.prototype.init_pointer = function () {
        console.log("init no mouse pointer"); //hide mouse
        var _document = document; //this will try to remove error from typescript
        //var blocker = _document.getElementById( 'blocker' );
        //var instructions = _document.getElementById( 'instructions' );
        var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
        function reqestpointer() {
            element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
            if (/Firefox/i.test(navigator.userAgent)) {
                var fullscreenchange = function (event) {
                    if (_document.fullscreenElement === element || _document.mozFullscreenElement === element || _document.mozFullScreenElement === element) {
                        _document.removeEventListener('fullscreenchange', fullscreenchange);
                        _document.removeEventListener('mozfullscreenchange', fullscreenchange);
                        element.requestPointerLock();
                    }
                };
                document.addEventListener('fullscreenchange', fullscreenchange, false);
                document.addEventListener('mozfullscreenchange', fullscreenchange, false);
                element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
                element.requestFullscreen();
            }
            else {
                element.requestPointerLock();
                console.log("click requestPointerLock");
            }
        }
        var idfocus = document.getElementById('focus');
        idfocus.addEventListener("click", function (e) {
            reqestpointer();
        }, false);
        //document.addEventListener("keydown", doKeyDown, false);
        document.addEventListener("keyup", doKeyDown, false);
        //window.addEventListener( "keypress", doKeyDown, false );
        function doKeyDown(e) {
            //console.log( e.keyCode );
            if (e.keyCode == 220) {
                reqestpointer();
            }
        }
        if (havePointerLock) {
            var element = _document.body; //this will try to remove error from typescript
            var pointerlockchange = function (event) {
                if (_document.pointerLockElement === element || _document.mozPointerLockElement === element || _document.webkitPointerLockElement === element) {
                }
                else {
                    //fpscontrol.enabled = false;
                    console.log(" fpscontrol false");
                }
            };
            var pointerlockerror = function (event) {
                //instructions.style.display = '';
            };
            // Hook pointer lock state change events
            document.addEventListener('pointerlockchange', pointerlockchange, false);
            document.addEventListener('mozpointerlockchange', pointerlockchange, false);
            document.addEventListener('webkitpointerlockchange', pointerlockchange, false);
            document.addEventListener('pointerlockerror', pointerlockerror, false);
            document.addEventListener('mozpointerlockerror', pointerlockerror, false);
            document.addEventListener('webkitpointerlockerror', pointerlockerror, false);
        }
        else {
        }
    };
    //===================================================================
    //player handlers section
    //===================================================================
    ThreejsGame.prototype.CreatePlayerObject = function (data) {
        var geometry = new THREE.BoxGeometry(1, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var player = new THREE.Mesh(geometry, material);
        player.userid = data['userid'];
        player.name = "playercube";
        this.scene.add(player);
        this.currentplayer = player;
    };
    return ThreejsGame;
})(ThreejsBase);
//init game
var game = new ThreejsGame();
//console.log(game);
